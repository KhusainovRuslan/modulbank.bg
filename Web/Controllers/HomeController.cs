using System;
using System.Linq;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to DevExpress Extensions for ASP.NET MVC!";
            return View();
        }

        public ActionResult DataBindingToLargeDatabasePartial()
        {
            return PartialView("~/Views/Partials/_shopWindowGridView.cshtml");
        }

        public ActionResult DataBindingToLargeDatabasePartialMaster()
        {
            return PartialView("~/Views/Partials/_detailGridView.cshtml");
        }

        public ActionResult RenderMasterGuarantee(string id)
        {
            ViewData["CustomerID"] = id;
            return PartialView("~/Views/Partials/_guaranteeMasterDetail.cshtml");
        }

        public ActionResult GuaranteeInfo(string id)
        {
            ViewData["CustomerID"] = id;
            return PartialView("~/Views/Partials/_guaranteeInfo.cshtml", LargeDatabaseDataProvider.Guarantees.First(x => x.Id.Equals(Guid.Parse(id))));
        }

        public ActionResult BeneficiaryInfo(string id)
        {
            var beneficiary = LargeDatabaseDataProvider.Guarantees.First(x => x.Id.Equals(Guid.Parse(id))).Beneficiary;
            ViewData["CustomerID"] = id;
            return PartialView("~/Views/Partials/_beneficiaryView.cshtml", beneficiary);
        }

        public ActionResult PrincipalInfo(string id)
        {
            var principal = LargeDatabaseDataProvider.Guarantees.First(x => x.Id.Equals(Guid.Parse(id))).Principal;
            ViewData["CustomerID"] = id;
            return PartialView("~/Views/Partials/_principalView.cshtml", principal);
        }

        public ActionResult Representatives(string id)
        {
            var representatives = LargeDatabaseDataProvider.Guarantees.First(x => x.Id.Equals(Guid.Parse(id))).Principal.Representatives;
            ViewData["CustomerID"] = id;
            return PartialView("~/Views/Partials/_representativesGridView.cshtml", representatives);
        }

        public ActionResult Files(string id)
        {
            var files = LargeDatabaseDataProvider.Guarantees.First(x => x.Id.Equals(Guid.Parse(id))).Files;
            ViewData["CustomerID"] = id;
            return PartialView("~/Views/Partials/_filesGridView.cshtml", files);
        }
    }
}