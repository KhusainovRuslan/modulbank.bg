﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Web;

namespace Web.Models
{
    public static class LargeDatabaseDataProvider
    {
        public static IQueryable<Guarantee> Guarantees;

        private static Random _rnd = new Random();

        private static readonly string[] Comments =
        {
            "Приложите пожалуйста паспорт Рогова Александра Витальевича",
            "Приложите пожалуйста выписку из реестра акционеров, заверенную не позднее прошлого понедельника этого года но прошлого месяца",
            "",
            "Проект не готов",
            "Заявление не заполнено, заполните и приложите",
            "Мы отказали вам в гарантии",
            "",
            "Васильева Н. Н. : не верно указан срок гарантии и проекта и прочего",
            "",
            "Выписка пуста",
            "Укажите дату рождения",
            "Lorem Ipsum - это текст-рыба, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной рыбой для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн.",
            "Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв",
            "",
            "Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации",
            "",
            "Классический текст Lorem Ipsum, используемый с XVI века, приведён ниже.",
            "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti",
            "On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot",
            "These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we",
        };

        private static readonly string[] Principals =
        {
            "ООО Рога и Копыта",
            "ООО Вектор",
            "АО Газпрм",
            "МУП Общеобразовательная школа №13 города Сызрани Самарской области",
            "ИП Галиев Вениамин Витальевич",
            "ООО Лента",
            "ООО Мастер на час",
            "ООО Электромонтаж-Сервсис",
            "ООО Принт",
            "ИП Гималайкина Анна Петровна",
            "ОАО Сбербанк-технолонии"
        };

        private static readonly string[] Statuses =
        {
            "Проверка ЮЛ",
            "Проверка ФЛ",
            "Выдано",
            "Ввод данных",
            "Ожидание финансовых документов",
            "Проверка проекта",
            "Ожидание оплаты",
            "Ожидание проекта",
        };

        static LargeDatabaseDataProvider()
        {
            var guarantees = new List<Guarantee>();

            var count = _rnd.Next(120, 499);
            for (var i = 0; i < count; i++)
            {
                var g = new Guarantee
                {
                    Id = Guid.NewGuid(),
                    Number = $"{i + 212000}",

                    Amount = _rnd.Next(150000, 8000000),
                    Commission = _rnd.Next(999, 125000),

                    Description = Comments[_rnd.Next(0, Comments.Length - 1)],
                    VerDescription = Comments[_rnd.Next(0, Comments.Length - 1)],

                    ModifiedOn = DateTime.Now.AddDays(_rnd.Next(-50, 0)),
                    IsPay = GetBool(),

                    Priority = GetBool() ? 1 : 0,
                    PrincipalName = Principals[_rnd.Next(0, Principals.Length - 1)],
                    Status = Statuses[_rnd.Next(0, Statuses.Length - 1)],
                    Ogrn = GetOgrn(),

                    Principal = GetPrincipal(),
                    Beneficiary = GetBeneficiary(),

                    IsManyHeaders = GetBool(),
                    Sla = _rnd.Next(100, 3600),
                };

                for (var j = 1; j < _rnd.Next(5, 25); j++)
                {
                    g.Files.Add(GetFile());
                }

                guarantees.Add(g);
            }

            Guarantees = guarantees.AsQueryable();
        }

        private static string GetOgrn() => $"{_rnd.Next(1000000, 1300000)}{_rnd.Next(3333333, 9999999)}";

        public static IEnumerable<Guarantee> GetGuarantees(ListEditItemsRequestedByFilterConditionEventArgs args)
        {
            var skip = args.BeginIndex;
            var take = args.EndIndex - args.BeginIndex + 1;
            var query = (from g in Guarantees
                         where g.Number.Contains(args.Filter)
                         orderby g.Number
                         select g
                ).Skip(skip).Take(take);
            return query.ToList();
        }

        public static Guarantee GetGuaranteeById(ListEditItemRequestedByValueEventArgs args)
        {
            if (args.Value == null || !Guid.TryParse(args.Value.ToString(), out var id))
                return null;
            return Guarantees.Where(p => p.Id == id).Take(1).SingleOrDefault();
        }

        private static readonly string[] RepresentativeNames =
        {
            "Петров Иван Петрович",
            "Оглы-Майорский Дмитрий Алексеевич",
            "Иванов Иван Иванович",
            "Морозко Мороз Новосибирцович",
            "Лампадов Сергей Третьякович",
            "Иогигабидзде Елена Севостьяновна",
            "Воробьева Алена Николаевна",
            "Терентьев Андрей Эдурдович",
        };

        private static readonly string[] JobTitles =
        {
            "Директор",
            "Врач",
            "Уборщица",
            "Оператор отдела",
            "ХТМ",
            "ГТМ",
            "Руководитель",
            "Президент",
        };

        private static bool GetBool() => _rnd.Next(100) <= 40;
        private static string GetInn() => _rnd.Next(100000000, 200000000).ToString();
        private static string GetPhone() =>
            $"+7 (9{_rnd.Next(0, 99)})-{_rnd.Next(100, 999)}-{_rnd.Next(10, 99)}-{_rnd.Next(10, 99)}";

        public static Representative GetRepresentative() => new Representative
        {
            Name = RepresentativeNames[_rnd.Next(0, RepresentativeNames.Length - 1)],
            FbrName = RepresentativeNames[_rnd.Next(0, RepresentativeNames.Length - 1)],
            ContactName = RepresentativeNames[_rnd.Next(0, RepresentativeNames.Length - 1)],

            IsHeader = GetBool(),
            IsDirector = GetBool(),
            IsSigned = GetBool(),
            IsArhvie = GetBool(),
            IsEmailDenied = GetBool(),
            IsViewPassport = GetBool(),
            Operationrestrictions = GetBool(),

            StartPower = DateTime.UtcNow.AddMonths(_rnd.Next(-60, 0)),
            EndPower = DateTime.UtcNow.AddMonths(_rnd.Next(-10, 60)),

            Share = _rnd.Next(25, 100),

            JobTitle = JobTitles[_rnd.Next(0, JobTitles.Length - 1)],
            AbsJob = JobTitles[_rnd.Next(0, JobTitles.Length - 1)],

            Protocol = $"{_rnd.Next(100, 999)}/2020-{_rnd.Next(1000000, 99999999)}",
            Snils = $"{_rnd.Next(100, 200)}-{_rnd.Next(10, 99)}-{_rnd.Next(100, 999)}-{_rnd.Next(1000, 8888)}",
            Inn = GetInn(),

            PassportNumber = _rnd.Next(100000, 999999).ToString(),
            PassportSeries = _rnd.Next(1000, 9999).ToString(),
            PassportWhen = DateTime.UtcNow.AddMonths(_rnd.Next(-90, -10)),
            PassportTo = DateTime.UtcNow.AddMonths(_rnd.Next(10, 150)),

            Phone = GetPhone(),
            Email = "emailemailemailemail@email.ru",
        };

        private static readonly string[] OpfTitles =
        {
            "ИП",
            "ООО",
            "ОАО",
            "ЗАО",
            "АО",
            "КФХ",
            "ИП",
            "ООО",
            "ОАО",
            "ЗАО",
            "АО",
            "КФХ",
        };

        private static readonly string[] SnoTitles =
        {
            "ЕНВД","УСН 6","УСН 15","ЕНВД","Патент", "УСН 6","УСН 15",
        };

        public static Principal GetPrincipal()
        {
            var principal = new Principal
            {
                Name = Principals[_rnd.Next(0, Principals.Length - 1)],
                Opf = OpfTitles[_rnd.Next(0, OpfTitles.Length - 1)],
                Sno = SnoTitles[_rnd.Next(0, SnoTitles.Length - 1)],
                Number = GetInn(),
                DateReg = DateTime.UtcNow.AddMonths(_rnd.Next(-90, -10)),
                DateRegNal = DateTime.UtcNow.AddMonths(_rnd.Next(-90, -10)),
                Inn = GetInn(),
                Ogrn = GetOgrn(),
                Kpp = GetInn(),
                Okato = GetInn(),
                Oktmo = GetInn(),
                Okpo = GetInn(),
                PayCapital = _rnd.Next(10000000, int.MaxValue),
                MainCapital = _rnd.Next(10000000, int.MaxValue),
                NumbersPeople = _rnd.Next(10, 10000),
                Phone = GetPhone(),
                Account = GetOgrn() + GetOgrn(),
                CorrAccount = GetOgrn() + GetOgrn(),
                Bik = GetInn(),
                Bank = GetBool() ? "АО Сбербанк" : "АО КБ Альфа-банк",
            };

            for (var i = 1; i < _rnd.Next(2, 5); i++)
            {
                principal.Representatives.Add(GetRepresentative());
            }

            return principal;
        }


        private static readonly string[] FileNames =
        {
            "Выписка.docx", "Справка.docx", "Паспрорт первый разворот.docx", "Паспрорт второй разворот.docx","Проект с правками клиента.docx","Проект с правками банка.docx","Проект на выпуск.docx",
            "Выписка.docx", "Справка.docx", "Паспрорт первый разворот.docx", "Паспрорт второй разворот.docx","Проект с правками клиента.docx","Проект с правками банка.docx","Проект на выпуск.docx"
        };

        private static readonly string[] FileTypeNames =
        {
            "Выписка", "Справка", "Паспрорт первый разворот", "Паспрорт второй разворот","Проект с правками клиента","Проект с правками банка","Проект на выпуск",
            "Выписка", "Справка", "Паспрорт первый разворот", "Паспрорт второй разворот","Проект с правками клиента","Проект с правками банка","Проект на выпуск"
        };

        public static Beneficiary GetBeneficiary() => new Beneficiary
        {
            FullName = Principals[_rnd.Next(0, Principals.Length - 1)],
            ShortName = Principals[_rnd.Next(0, Principals.Length - 1)],
            Opf = OpfTitles[_rnd.Next(0, OpfTitles.Length - 1)],
            DateReg = DateTime.UtcNow.AddMonths(_rnd.Next(-90, -10)),
            Account = GetOgrn() + GetOgrn(),
            CorrAccount = GetOgrn() + GetOgrn(),
            Bik = GetInn(),
            Bank = GetBool() ? "АО Сбербанк" : "АО КБ Альфа-банк",

            Arbitrage = "Костромской арбитражный суд",
            Address = "630008 обл Новосибирская, г Новосибирск, ул Ленина, д. 102 кв. 105 стр. 1",
            Inn = GetInn(),
            Kpp = GetInn(),
            KppLarge = GetInn(),
            Ogrn = GetOgrn(),
        };

        public static FileInfo GetFile() => new FileInfo
        {
            Name = FileNames[_rnd.Next(0, FileNames.Length - 1)],
            FileType = FileTypeNames[_rnd.Next(0, FileTypeNames.Length - 1)],
            A = GetBool(),
            Ekb = GetBool(),
            Ekc = GetBool(),
            Date = DateTime.UtcNow.AddHours(_rnd.Next(-500, 0)),
            Source = GetBool() ? "ЛК" : "CRM",
            Url = "https://dxnav.landrover.com/legacy/images/logo-landrover-7bd33da9d247934b2b4929d399ab8a42.png",
        };
    }
}