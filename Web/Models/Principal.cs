﻿using System;
using System.Collections.Generic;

namespace Web.Models
{
    public class Principal
    {
        public Principal()
        {
            Representatives = new List<Representative>();
        }

        public Guid Id { set; get; } = Guid.NewGuid();
        public List<Representative> Representatives { set; get; }
        private string _name;

        public string Name
        {
            set => _name = value;
            get => _name;
        }

        public string FullName => _name;
        public string ShortName => _name;
        public string Opf { set; get; }
        public string Sno { set; get; }
        public string Number { set; get; }
        public DateTime DateReg { set; get; }
        public string NameReg { set; get; } = "ОУФМС Красноуфимского райнного села Центрального района";
        public string MainOkved { set; get; } = "Услуги перевозок по стране";
        public string Inn { set; get; }
        public string Ogrn { set; get; }
        public string Kpp { set; get; }
        public string Okato { set; get; }
        public string Oktmo { set; get; }
        public string Okpo { set; get; }
        public int PayCapital { set; get; }
        public int MainCapital { set; get; }
        public int NumbersPeople { set; get; }
        public DateTime DateRegNal { set; get; }
        public string Phone { set; get; }

        public string Address { set; get; } = "630008 обл Новосибирская, г Новосибирск, ул Ленина, д. 102 кв. 105 стр. 1";
        public string Fias { set; get; } = "016501650896189604180";
        public string Country { set; get; } = "Россия";
        public string Index { set; get; } = "630008";
        public string Region { set; get; } = "обл Новосибирская";
        public string Area { set; get; } = "Новосибирский";
        public string City { set; get; } = "Новосибирск";
        public string Street { set; get; } = "Новосибирская";
        public string Home { set; get; } = "120";
        public string Build { set; get; } = "5";
        public string Corpus { set; get; } = "2П";
        public string Flat { set; get; } = "050";
        public string Liter { set; get; } = "П";
        public string Office { set; get; } = "60";

        public string Bik { set; get; }
        public string Bank { set; get; }
        public string Account { set; get; }
        public string CorrAccount { set; get; }
    }
}