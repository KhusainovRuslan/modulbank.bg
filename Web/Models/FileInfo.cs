﻿using System;

namespace Web.Models
{
    public class FileInfo
    {
        public Guid Id { set; get; } = Guid.NewGuid();
        public string Name { set; get; }
        public string Url { set; get; }
        public bool Ekc { set; get; }
        public bool Ekb { set; get; }
        public bool A { set; get; }
        public string FileType { set; get; }
        public DateTime Date { set; get; }
        public string Source { set; get; }
    }
}