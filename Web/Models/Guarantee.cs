﻿using System;
using System.Collections.Generic;

namespace Web.Models
{
    public class Guarantee
    {
        public Guarantee()
        {
            Files = new List<FileInfo>();
        }

        public Guid Id { set; get; }
        public string Number { set; get; }
        public string PrincipalName { set; get; }
        public string Ogrn { set; get; }
        public string Status { set; get; }
        public string Description { set; get; }
        public string VerDescription { set; get; }
        public bool IsPay { set; get; }
        public int Priority { set; get; }
        public DateTime ModifiedOn { set; get; }
        public double Amount { set; get; }
        public double Commission { set; get; }

        public bool IsManyHeaders { set; get; }
        public int Sla { set; get; }

        public Beneficiary Beneficiary { set; get; }
        public Principal Principal { set; get; }
        public List<FileInfo> Files { set; get; }
    }
}