﻿using System;

namespace Web.Models
{
    public class Representative
    {
        public Guid Id { set; get; } = Guid.NewGuid();
        public string Name { set; get; }
        public string FbrName { set; get; }
        public double Share { set; get; }
        public string JobTitle { set; get; }
        public DateTime StartPower { set; get; }
        public DateTime EndPower { set; get; }
        public string Protocol { set; get; }
        public bool IsHeader { set; get; }
        public bool IsDirector { set; get; }
        public bool IsSigned { set; get; }
        public bool IsArhvie { set; get; }
        public bool IsEmailDenied { set; get; }
        public bool IsViewPassport { set; get; }

        public string AbsJob { set; get; }
        public string AbsLine { set; get; } = "Единоличный исполнительный орган";

        public string Snils { set; get; } 
        public string SignType { set; get; } = "Первая";
        public bool Operationrestrictions { set; get; }

        public string ContactName { set; get; }
        public string Inn { set; get; }
        public string PassportSeries { set; get; }
        public string PassportNumber { set; get; }
        public DateTime PassportWhen { set; get; }
        public DateTime PassportTo { set; get; }
        public string PassportWho { set; get; } = "город Момсква ОУФМС №2 Полк 1243214";
        public string Phone { set; get; }
        public string Email { set; get; }
    }
}