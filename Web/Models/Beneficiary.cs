﻿using System;

namespace Web.Models
{
    public class Beneficiary
    {
        public string FullName { set; get; }
        public string ShortName { set; get; }
        public string Opf { set; get; }
        public DateTime DateReg { set; get; }
        public string Inn { set; get; }
        public string Ogrn { set; get; }
        public string Kpp { set; get; }
        public string KppLarge { set; get; }
        public string Address { set; get; }
        public string Region { set; get; } = "Ханты-мансийский Автономный округ";

        public string Bik { set; get; }
        public string Bank { set; get; }
        public string Account { set; get; }
        public string CorrAccount { set; get; }
        public string Arbitrage { set; get; }
    }
}